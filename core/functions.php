<?php

function checkInArray(&$array, $key, $default=false){
    return (isset($array[$key])) ? $array[$key] : $default;
}

function check_array($array) {
    return (is_array($array) && !empty($array));
}

function _def($var,$def=false) {
    return (!$var || $var === NULL) ? $def : $var;
}

function _cleanVal($value){
    if(check_array($value)){return array();}
    if(is_string($value)){return '';}
    if(is_object($value)){return new stdClass();}
    if(is_int($value) || is_float($value)){return 0;}
    return false;
}

function _defFile($var,$def=false){
    return (!file_exists($var)) ? $def : $var;
}

function strToArray($str){
    if(is_string($str)){
        $res = array();
        array_push($res, $str);
        return $res;
    } else {
        return false;
    }
}

function is_email($data){
    if($data && is_string($data)){
        return (preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $data)) ? $data : false;
    } else {
        return false;
    }
}

function objectToArray($object){
    return (is_object($object)) ? (array) $object : false;
}

function fieldToArray($field){
    $res = array();

    if(check_array($field)){
        return $field;
    }
    if(is_string($field)){
        array_push($res, $field);
        return $res;
    }

    if(is_object($field)){
        return (array) $field;
    }
    if(is_int($field) || is_float($field)){
        array_push($res, $field);
        return $res;
    }
    return false;
}

function array_to_obj($array, &$obj)
{
    foreach ($array as $key => $value)
    {
        if (is_array($value))
        {
            $obj->$key = new stdClass();
            array_to_obj($value, $obj->$key);
        }
        else
        {
            $obj->$key = $value;
        }
    }
    return $obj;
}

function fieldToObj($field){
    $res = new stdClass();

    if(_def($field)){
        if(is_object($field)){
            return $field;
        } else if(check_array($field)){
            return array_to_obj($field);
        } else {
            $res->{0} = $field;
            return $res;
        }
    } else {
        return false;
    }
}

function isParseInt($str){
    return (preg_match("^[0-9]{1,11}$",$str)) ? true : false;
}

function vdd($data=false, $die = false) {
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
    if($die){
        die;
    }
}

function newArray(){
    return array();
}

function getCurrentDate(){
    return date("Y-m-d H:i:s");
}

function dateShowFormat($data){
    if($data && is_string($data) && strlen($data) == 10){
        $data = explode("-", $data);
        if(count($data) == 3){
            return $data[2] . '.' . $data[1] . '.' . $data[0];
        } else {
            return '';
        }
    } else {
        return '';
    }
}

function setNextPage ($page=false,$slashes=true,$CurServer=true,$protocol=HOST_PROTOCOL) {
    $Server = ($CurServer) ? $_SERVER['HTTP_HOST'].(($slashes)?'/':false) : false;
    $Link = $protocol.$Server.$page;
    header("location: ".$Link);
}