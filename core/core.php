<?php

namespace Core;
require_once(CORE_DIR . S . 'functions.php');

class Core{
    public $data = array();
    private $UrlArray = false;
    private $Action = false;
    private $isApi = false;

    public function __construct() {
        $this->getServerData();
        $this->getRequestArray(); // парсим Url и разбиваем по сегментах
        $this->getActions(); // определяем что запустить апи или статику
        $this->loadCoreFiles(); // запускаем скрипт
    }

    public function __set($name,$value){
        $this->data[$name] = $value;
        return $value;
    }

    public function __get($name){
        return (isset($this->data[$name])) ? $this->data[$name] : false;
    }

    private function getServerData(){
        $this->SiteUrl = $_SERVER['HTTP_HOST'];
        $this->RequestUrl = $_SERVER["REQUEST_URI"];
        $this->Localhost = $_SERVER["SERVER_ADDR"];
    }

    private function getRequestArray(){
        $SiteRequestUrl = HOST_PROTOCOL.$this->SiteUrl.$this->RequestUrl;
        $UrlArray = parse_url($SiteRequestUrl);
        $UrlArray = checkInArray($UrlArray,'path');
        $UrlArray = explode('/',_def($UrlArray,array()));
        $UrlArray = array_filter($UrlArray);
        $this->UrlArray = array_values($UrlArray);
    }

    private function getActions(){
        $url_array = $this->UrlArray;

        if($url_array[0] == API_URL_SEGMENT){
            $this->isApi = true;
            unset($url_array[0]);
            $url_array = array_values($url_array);
            if(check_array($url_array)){
                $this->Action = $url_array[0];
                unset($url_array[0]);
                $url_array = array_values($url_array);
            }
        }
        $this->UrlArray = $url_array;
    }

    private function loadCoreFiles(){
        if($this->isApi){
            require_once(API_DIR . S . 'apiController.php');
        }
    }

    public function run(){
        if($this->isApi){
            if(file_exists(API_DIR . S . 'controllers' . S . $this->Action . '.php')){
                require_once(API_DIR . S . 'controllers' . S . $this->Action . '.php');
                $ActionLoad = '\\api\\' . $this->Action . 'Controller';
                new $ActionLoad($this->UrlArray);
            } else {
                vdd(404, true);
                setNextPage('404');
            }
        } else {
            require_once(PUBLIC_DIR . S . 'index.html');
        }
    }

}