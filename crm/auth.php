<?php
namespace crm;
// клас авторизации
final class Auth{
    private $data;
    private $user;
    private $login;
    private $hash;
    private $sub;

    private function __construct($login, $hash, $sub){
        $this->login = $login;
        $this->hash = $hash;
        $this->sub = $sub;
        if(!_def($this->user)){
            $this->login();
        }
    }

    public static function getInstance(){
        static $inst = null;
        if ($inst === null) {
            $inst = new Auth(CRM_USER_LOGIN, CRM_USER_HASH, CRM_SUB);
        }
        return $inst;
    }

    private function __clone(){}

    private function __wakeup(){}

    public function __set($name,$value){
        $this->data[$name] = $value;
        return $value;
    }

    public function __get($name){
        return (isset($this->data[$name])) ? $this->data[$name] : false;
    }

    private function login(){
        $user=array(
            'USER_LOGIN'=>$this->login,
            'USER_HASH'=>$this->hash
        );
        $link='https://'.$this->sub.'.amocrm.ru/private/api/auth.php?type=json';
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($user));
        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        $out=curl_exec($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);
        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try {
            if($code!=200 && $code!=204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
        } catch(Exception $E) {
            die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
        }

        $Response=json_decode($out,true);
        $Response=$Response['response'];
        if(isset($Response['auth'])) $this->user = $Response['auth'];
    }

    public function getUser(){
        return $this->user;
    }
}