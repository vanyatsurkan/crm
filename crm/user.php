<?php
namespace crm;
include(ROOT_DIR . S . 'crm' . S . 'auth.php');

class User{
    private $auth;

    public function __construct(){
        $auth = Auth::getInstance();
        $this->auth = $auth->getUser();
    }

    public function getUsers()// получаем список юзеров
    {
        if(_def($this->auth)){
            $link='https://'.CRM_SUB.'.amocrm.ru/api/v2/account?with=users';
            $curl=curl_init();
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
            curl_setopt($curl,CURLOPT_URL,$link);
            curl_setopt($curl,CURLOPT_HEADER,false);
            curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
            curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
            $out=curl_exec($curl);
            $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
            curl_close($curl);
            $code=(int)$code;
            $errors=array(
                301=>'Moved permanently',
                400=>'Bad request',
                401=>'Unauthorized',
                403=>'Forbidden',
                404=>'Not found',
                500=>'Internal server error',
                502=>'Bad gateway',
                503=>'Service unavailable'
            );
            try
            {
                if($code!=200 && $code!=204) {
                    throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
                }
            }
            catch(Exception $E)
            {
                die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
            }
            $Response=json_decode($out,true);
            return $Response['_embedded']['users'];
        } else {
            return false;
        }
    }
}