function validateEmail(email) {
    let pattern  = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email);
}

function validatePhone(phone) {
    let pattern = /^\d[\d\(\)\ -]{4,14}\d$/;
    return pattern.test(phone);
}

function validateName(name) {
    let pattern = /^[a-zA-Zа-яА-Я'][a-zA-Zа-яА-Я-' ]+[a-zA-Zа-яА-Я']?$/u;
    return pattern.test(name);
}


$('body')
    .on('submit', 'form[name="crm"]', function(e){
        e.preventDefault();
        const form = $(this);
        if(!validateName($('input[name="name"]').val())){
            $('.name-error').html('Name is not valid!');
            setTimeout(() => {
                $('.name-error').html('');
            }, 2000)
        } else if(!validatePhone($('input[name="phone"]').val())){
            $('.phone-error').html('Phone is not valid!');
            setTimeout(() => {
                $('.phone-error').html('');
            }, 2000)
        } else if(!validateEmail($('input[name="email"]').val())){
            $('.email-error').html('Email is not valid!');
            setTimeout(() => {
                $('.email-error').html('');
            }, 2000)
        } else {
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (data) {
                    if(data.status){
                        alert('Success!');
                        form.find('input[name="name"]').val('');
                        form.find('input[name="phone"]').val('');
                        form.find('input[name="email"]').val('');
                    } else {
                        alert('Error! ' + data.error);
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
            return false;
        }
    });