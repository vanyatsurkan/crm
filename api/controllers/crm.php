<?php
namespace api;
include(ROOT_DIR . S . 'crm' . S . 'user.php');
include(ROOT_DIR . S . 'crm' . S . 'contact.php');
include(ROOT_DIR . S . 'crm' . S . 'lead.php');
include(ROOT_DIR . S . 'crm' . S . 'task.php');
use \api\apiController as aCtrl;
use crm\User as User;
use crm\Contact as Contact;
use crm\Lead as Lead;
use crm\Task as Task;

class crmController extends aCtrl{
    public function __construct($urlArray)
    {
        parent::__construct($urlArray);
        $action = $this->ActionRun;
        if (method_exists($this, $action)) {
            $this->$action();
        } else {
            $this->setAnswerCode(404);
            $this->answer();
            die;
        }
    }

    private function post(){
        // Принимаем POST параметры
        if(_def($this->RequestParams['name']) && _def($this->RequestParams['email']) && _def($this->RequestParams['phone'])){
            $contact = new Contact($this->RequestParams['name'], $this->RequestParams['email'], $this->RequestParams['phone']); // создаем екземпляр  класа Contact
            $contactInfo = $contact->getInfo(); // если контакт существует, получаем информацию о контакте
            $createContact = false;
            $responsible_user_id = 0;

            if(_def($contactInfo)&& _def($contactInfo[0])){
                $responsible_user_id = $contactInfo[0]['responsible_user_id']; // если контакт существует, отвественного за сделку назначаем существующий контакт
            } else {
                $createContact = true;
                $user = new User();
                $users = $user->getUsers();  // получаем список юзеров
                $responsibleArray = [];   // массив юзеров с количесвом сделок за сегодня
                foreach($users as $value){
                    if(!$value['is_admin']){
                        $responsibleArray[$value['id']] = 0;
                    }
                }
                $lead = new Lead();
                $leads = $lead->getLeads();   // получаем список слелок
                if(count($leads) > 0){
                    for($i=0;$i<count($leads);$i++){   // вычисляем сколько сделок за сегодня у каждого юзера
                        if((int)$leads[$i]['created_at'] > time()- 24*60*60 && _def($responsibleArray[$leads[$i]['responsible_user_id']])){
                            $responsibleArray[$leads[$i]['responsible_user_id']] += 1;
                        }
                    }
                }

                foreach($responsibleArray as $key => $value){   // проверяем у кого меньше сделок и назначаем его отвественного
                    if(!_def($responsible_user_id)){
                        $responsible_user_id = $key;
                    } else {
                        if($responsibleArray[$responsible_user_id] && $responsibleArray[$key] < $responsibleArray[$responsible_user_id]){
                            $responsible_user_id = $key;
                        }
                    }
                }
            }

            $lead = new Lead();
            $addLead = $lead->addLead($responsible_user_id);   // добовляем сделку
            if($createContact){  // песли контакт не существовал, добовляем его
                $contactInfo = $contact->addContact($addLead['id']);
            }

            $task = new Task();
            $res = $task->addCallTask($responsible_user_id, $contactInfo['id']);   // создаем задачу

            $this->setQueryStatus(true);
            $this->setQueryAnswer($res, true);
            $this->Answer();   // ответ апи
        } else {
            $this->setQueryStatus(false);
            $this->Answer();
        }
    }


}