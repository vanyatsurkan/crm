<?php
namespace api;
use \api\apiController as aCtrl;

class testController extends aCtrl{
    public function __construct($urlArray)
    {
        parent::__construct($urlArray);
        $action = $this->ActionRun;
        if (method_exists($this, $action)) {
            $this->$action();
        } else {
            $this->setAnswerCode(404);
            $this->answer();
            die;
        }
    }

    private function get(){
//        $test = new Contact();
//        $test1 = new User();
        $this->setQueryStatus(true);
        $this->Answer();
    }


}