<?php
/**
 * Created by PhpStorm.
 * User: Vanya Tsurkan
 * Date: 01.02.17
 * Time: 12:20
 */

namespace api;

class apiController
{
    public $Data = array();
    public $Actions = false;
    public $ActionRun = false;
    public $Config = array();
    public $RequestParams = false;
    public $RequestModelId = false;
    public $RequestMethod = false;
    public $Db;

    private $QueryStatus = true;
    private $QueryAnswer = false;
    private $QueryError = false;
    private $QuerySystem = false;
    private $QueryAnswerCount = false;
    private $QueryAnswerTexts = false;
    private $QueryAnswerPermission = false;
    private $QueryCode = 200;

    public function __construct($Actions = false)
    {
        $this->getMethod();
        $this->getRequestActions($Actions);
        $this->getRequestParams();
        $this->getActionRun();
    }

    public function __set($name, $value)
    {
        $this->Data[$name] = $value;
        return $value;
    }

    public function __get($name)
    {
        return (isset($this->Data[$name])) ? $this->Data[$name] : false;
    }


    private function getMethod()
    {
        $this->RequestMethod = $_SERVER['REQUEST_METHOD'];
    }

    private function getRequestParams()
    {
        if (strripos($_SERVER['CONTENT_TYPE'], 'application/json') !== false) {
            $request = file_get_contents('php://input');
            $this->RequestParams = fieldToArray(json_decode($request, true));
        } else {
            if ($this->RequestMethod == "GET") {
                $this->RequestParams = $_GET;
            } else if ($this->RequestMethod == "POST") {
                $this->RequestParams = $_POST;
            } else {
                $request = file_get_contents('php://input');
                if ($request && $request != "") {
                    parse_str($request, $this->RequestParams);
                }
            }
        }
    }

    private function getRequestActions($actions)
    {
        $this->Actions = $actions;
    }

    private function httpResponseCode($code)
    {
        if (_def($code)) {
            switch ($code) {
                case 100:
                    header("HTTP/1.1 100 Continue");
                    break;
                case 101:
                    header("HTTP/1.1 101 Switching Protocols");
                    break;
                case 200:
                    header("HTTP/1.1 200 OK");
                    break;
                case 201:
                    header("HTTP/1.1 201 Created");
                    break;
                case 202:
                    header("HTTP/1.1 202 Accepted");
                    break;
                case 203:
                    header("HTTP/1.1 203 Non-Authoritative Information");
                    break;
                case 204:
                    header("HTTP/1.1 204 No Content");
                    break;
                case 205:
                    header("HTTP/1.1 205 Reset Content");
                    break;
                case 206:
                    header("HTTP/1.1 206 Partial Content");
                    break;
                case 300:
                    header("HTTP/1.1 300 Multiple Choices");
                    break;
                case 301:
                    header("HTTP/1.1 301 Moved Permanently");
                    break;
                case 302:
                    header("HTTP/1.1 302 Moved Temporarily");
                    break;
                case 303:
                    header("HTTP/1.1 303 See Other");
                    break;
                case 304:
                    header("HTTP/1.1 304 Not Modified");
                    break;
                case 305:
                    header("HTTP/1.1 305 Use Proxy");
                    break;
                case 400:
                    header("HTTP/1.1 400 Bad Request");
                    die;
                    break;
                case 401:
                    header("HTTP/1.1 401 Unauthorized");
                    break;
                case 402:
                    header("HTTP/1.1 402 Payment Required");
                    break;
                case 403:
                    header("HTTP/1.1 403 Forbidden");
                    die;
                    break;
                case 404:
                    header("HTTP/1.1 404 Not Found");
                    die;
                    break;
                case 405:
                    header("HTTP/1.1 405 Method Not Allowed");
                    break;
                case 406:
                    header("HTTP/1.1 406 Not Acceptable");
                    break;
                case 407:
                    header("HTTP/1.1 407 Proxy Authentication Required");
                    break;
                case 408:
                    header("HTTP/1.1 408 Request Time-out");
                    break;
                case 409:
                    header("HTTP/1.1 409 Conflict");
                    break;
                case 410:
                    header("HTTP/1.1 410 Gone");
                    break;
                case 411:
                    header("HTTP/1.1 411 Length Required");
                    break;
                case 412:
                    header("HTTP/1.1 412 Precondition Failed");
                    break;
                case 413:
                    header("HTTP/1.1 413 Request Entity Too Large");
                    break;
                case 414:
                    header("HTTP/1.1 414 Request-URI Too Large");
                    break;
                case 415:
                    header("HTTP/1.1 415 Unsupported Media Type");
                    break;
                case 500:
                    header("HTTP/1.1 500 Internal Server Error");
                    die;
                    break;
                case 501:
                    header("HTTP/1.1 501 Not Implemented");
                    break;
                case 502:
                    header("HTTP/1.1 502 Bad Gateway");
                    break;
                case 503:
                    header("HTTP/1.1 503 Service Unavailable");
                    break;
                case 504:
                    header("HTTP/1.1 504 Gateway Time-out");
                    break;
                case 505:
                    header("HTTP/1.1 505 HTTP Version not supported");
                    break;
                default:
                    header("HTTP/1.1 200 OK");
                    break;
            }
        }
    }

    private function getActionRun()
    {
        $action_run = strtolower($this->RequestMethod);
        foreach ($this->Actions as $value) {
            $action_run .= ucfirst(strtolower($value));
        }
        $this->ActionRun = $action_run;
    }

    public function setAnswerCode($code)
    {
        $this->QueryCode = $code;
    }

    public function setQuerySystem($system)
    {
        $this->QuerySystem = $system;
    }

    public function setQueryAnswer($answer, $isarray = false)
    {
        if ($isarray) {
            $this->QueryAnswer = fieldToArray($answer);
        } else {
            $this->QueryAnswer = fieldToObj($answer);
        }
    }

    public function setQueryAnswerCount($count)
    {
        if (isParseInt($count)) {
            $this->QueryAnswerCount = $count;
        }
    }

    public function setQueryAnswerPermission($arr)
    {
        if (check_array($arr)) {
            $this->QueryAnswerPermission = $arr;
        }
    }

    public function setQueryAnswerTexts($texts)
    {
        if (_def($texts)) {
            $this->QueryAnswerTexts = fieldToArray($texts);
        }
    }

    public function setQueryStatus($status)
    {
        $this->QueryStatus = ($status) ? true : false;
    }

    public function setQueryError($error)
    {
        $this->QueryError = ($error) ? $error : false;
    }

    public function setFailedAnswer($error = false, $system = false)
    {
        $this->setQueryStatus(false);
        if (_def($error)) {
            $this->setQueryError($error);
        }
        if (_def($system)) {
            $this->setQuerySystem($system);
        }
        $this->Answer();
    }

    public function Answer()
    {
        $answer = new \stdClass();
        $answer->status = $this->QueryStatus;
        if ($this->QueryStatus) {
            $answer->data = $this->QueryAnswer;
        } else {
            $answer->error = $this->QueryError;
        }
        if (_def($this->QueryAnswerCount)) {
            $answer->count = (int)$this->QueryAnswerCount;
        }
        if (check_array($this->QueryAnswerTexts)) {
            $answer->texts = $this->QueryAnswerTexts;
        }
        if (check_array($this->QueryAnswerPermission)) {
            $answer->permission = $this->QueryAnswerPermission;
        }

        $answer->system = $this->QuerySystem;

        $this->httpResponseCode($this->QueryCode);
        header('Content-Type: application/json');
        print json_encode($answer);
        die;
    }
}