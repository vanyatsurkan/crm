<?php
session_start();
define('ROOT_DIR',dirname(__FILE__));
require_once(ROOT_DIR . S . 'config' . S . 'config.php');
require_once(CORE_DIR . S . 'core.php');

$app = new \Core\Core();
$app->run();